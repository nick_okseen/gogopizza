@extends('adminlte::page')

@section('title', 'Товары')

@section('content_header')
    <h1>Товары</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Товары</h3>
                </div>
                <!-- /.box-header -->
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Название</th>
                                    <th class="text-center">30 см</th>
                                    <th class="text-center">45 см</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <td class="w-50">{{ $product->product_name }}</td>
                                        <td class="w-25 text-center">
                                            <input data-id="{{ $product->id }}" class="is-30" type="checkbox" {{ $product->is_30 ? 'checked' : '' }}>
                                        </td>
                                        <td class="w-25 text-center">
                                            <input data-id="{{ $product->id }}" class="is-45" type="checkbox" {{ $product->is_45 ? 'checked' : '' }}>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.is-30').change(function (){
                let value = $(this).prop('checked');
                let id = $(this).data('id');

                $.ajax({
                    url: '/admin/products/update-product',
                    type: 'post',
                    data: {
                        id: id,
                        is_30: value
                    },
                    success: function(){},
                    error: function(error){}
                });
            });

            $('.is-45').change(function (){
                let value = $(this).prop('checked');
                let id = $(this).data('id');

                $.ajax({
                    url: '/admin/products/update-product',
                    type: 'post',
                    data: {
                        id: id,
                        is_45: value
                    },
                    success: function(){},
                    error: function(error){}
                });
            });
        });
    </script>
@stop
