@extends('adminlte::page')

@section('title', 'Настройки')

@section('content_header')
    <h1>Настройки</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Настройки</h3>
                </div>
                <!-- /.box-header -->
                <div class="card-body">
                    <form action="{{ route('settings.store') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="pizza_category" class="col-12 col-form-label">Категория для пиццы</label>
                            <select name="pizza_category" class="form-control" id="pizza_category">
                                <option></option>
                                @foreach($categories as $category)
                                    @if(isset($pizza_category) && $pizza_category != false && $pizza_category == $category->category_id)
                                        <option value="{{ $category->category_id }}" selected>{{ $category->category_name }}</option>
                                    @else
                                        <option value="{{ $category->category_id }}">{{ $category->category_name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="water_category" class="col-12 col-form-label">Категории для напитков и кофе</label>
                            <select name="water_category[]" class="form-control select2-water" id="water_category" multiple>
                                @foreach($categories as $category)
                                    @if(isset($water_category) && $water_category != false && in_array($category->category_id, $water_category))
                                        <option value="{{ $category->category_id }}" selected>{{ $category->category_name }}</option>
                                    @else
                                        <option value="{{ $category->category_id }}">{{ $category->category_name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="without_border" class="col-12 col-form-label">Модификатор без бортика</label>
                            <select name="without_border" class="form-control" id="without_border">
                                <option></option>
                                @foreach($modifications as $modification)
                                    @if(isset($selectedWithoutBorder) && $selectedWithoutBorder != false && $selectedWithoutBorder == $modification->dish_modification_id)
                                        <option value="{{ $modification->dish_modification_id }}" selected>{{ $modification->name }}</option>
                                    @else
                                        <option value="{{ $modification->dish_modification_id }}">{{ $modification->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="border_30" class="col-12 col-form-label">Модификатор Бортик 30см</label>
                            <select name="border_30" class="form-control" id="border_30">
                                <option></option>
                                @foreach($modifications as $modification)
                                    @if(isset($selectedBorder30) && $selectedBorder30 != false && $selectedBorder30 == $modification->dish_modification_id)
                                        <option value="{{ $modification->dish_modification_id }}" selected>{{ $modification->name }}</option>
                                    @else
                                        <option value="{{ $modification->dish_modification_id }}">{{ $modification->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="border_45" class="col-12 col-form-label">Модификатор Бортик 45см</label>
                            <select name="border_45" class="form-control" id="border_45">
                                <option></option>
                                @foreach($modifications as $modification)
                                    @if(isset($selectedBorder45) && $selectedBorder45 != false  && $selectedBorder45 == $modification->dish_modification_id)
                                        <option value="{{ $modification->dish_modification_id }}" selected>{{ $modification->name }}</option>
                                    @else
                                        <option value="{{ $modification->dish_modification_id }}">{{ $modification->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="modifications" class="col-12 col-form-label">Модификаторы для допов</label>
                            <select name="modifications[]" class="form-control select2-modifications" id="modifications" multiple>
                                @foreach($modifications as $modification)
                                    @if(isset($selectedAdditional) && $selectedAdditional != false  && $selectedAdditional && in_array($modification->dish_modification_id, $selectedAdditional))
                                        <option value="{{ $modification->dish_modification_id }}" selected>{{ $modification->name }}</option>
                                    @else
                                        <option value="{{ $modification->dish_modification_id }}">{{ $modification->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $('#pizza_category').select2({
                placeholder: "Выберите категорию",
            });
            $('#water_category').select2({
                placeholder: "Выберите категории",
                allowClear: true
            });
            $('#without_border').select2({
                placeholder: "Выберите модификатор"
            });
            $('#border_30').select2({
                placeholder: "Выберите модификатор"
            });
            $('#border_45').select2({
                placeholder: "Выберите модификатор"
            });
            $('#modifications').select2({
                placeholder: "Вырерите модификаторы",
                allowClear: true
            });
        });
    </script>
@stop
