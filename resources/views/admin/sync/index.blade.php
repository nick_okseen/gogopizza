@extends('adminlte::page')

@section('title', 'Синхронизация')

@section('content_header')
    <h1>Синхронизация</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Синхронизация</h3>
                    <div class="card-tools">
                        <button class="btn btn-sm btn-success" id="syncAll">
                            <i class="fa fa-edit"></i> Синхронизировать все
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="card-body">

                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#syncAll').click(function (){
                $.ajax({
                    url: '/admin/sync-all',
                    type: 'get',
                    success: function(){
                        $('.box-body').append('<div class="text-success">Готово</div>');
                    },
                    beforeSend: function() {},
                    complete: function() {},
                    error: function(error){
                        $('.box-body').append('<div class="text-danger">'+error+'</div>');
                    }
                });
            });
        });
    </script>
@stop
