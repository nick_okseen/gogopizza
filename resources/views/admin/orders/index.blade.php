@extends('adminlte::page')

@section('title', 'Сделать заказ')

@section('content_header')
    <h1>Сделать заказ</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Сделать заказ</h3>
                </div>
                <!-- /.box-header -->
                <div class="card-body">
                    <a href="{{ route('orders.makeTestOrder') }}" class="btn btn-success">
                        <i class="fa fa-recycle"></i>
                        Сделать тестовый заказ
                    </a>

                    <a href="{{ route('orders.getPosterOrder') }}" class="btn btn-success">
                        <i class="fa fa-recycle"></i>
                        Получить инфу по заказу
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop
