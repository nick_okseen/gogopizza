@extends('adminlte::page')

@section('title', 'Ингредиенты')

@section('content_header')
    <h1>Ингредиенты</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Ингредиенты</h3>
                </div>
                <!-- /.box-header -->
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Название</th>
                                    <th class="text-center">Показывать в описании</th>
                                    <th class="text-right">Название для сайта</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($ingredients as $ingredient)
                                    <tr>
                                        <td class="w-25">{{ $ingredient->ingredient_name }}</td>
                                        <td class="w-25 text-center">
                                            <input data-id="{{ $ingredient->id }}" class="is-in-product-list" type="checkbox" {{ $ingredient->is_in_products_list ? 'checked' : '' }}>
                                        </td>
                                        <td class="w-50 text-right">
                                            <input style="width: 160px;" class="form-control d-inline-block w-50" type="text" value="{{ $ingredient->site_name ?? '' }}">
                                            <button style="width: 140px;" class="update-ingredient btn btn-success d-inline-block" data-id="{{ $ingredient->id }}">Сохранить</button>
                                            <div style="display: none;" class="success-icon"><i class="fa fa-check"></i></div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.is-in-product-list').change(function (){
                let value = $(this).prop('checked');
                let id = $(this).data('id');

                $.ajax({
                    url: '/admin/ingredients/update-ingredient',
                    type: 'post',
                    data: {
                        id: id,
                        is_in_products_list: value
                    },
                    success: function(){},
                    error: function(error){}
                });
            });

            $('.update-ingredient').click(function (){
                let parentNode = $(this).closest('td');
                let id = $(this).data('id');
                let name = parentNode.find('input').val();

                $.ajax({
                    url: '/admin/ingredients/update-ingredient',
                    type: 'post',
                    data: {
                        id: id,
                        site_name: name
                    },
                    success: function(){
                        parentNode.find('.success-icon').css('display', 'inline-block');

                        setTimeout(function(){
                            parentNode.find('.success-icon').css('display', 'none');
                        }, 1000);
                    },
                    error: function(error){
                        parentNode.find('input').css('border', '1px solid red');
                    }
                });
            });
        });
    </script>
@stop
