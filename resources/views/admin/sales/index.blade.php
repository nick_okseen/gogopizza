@extends('adminlte::page')

@section('title', 'Категории')

@section('content_header')
    <h1>Категории</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Категории</h3>
                </div>
                <!-- /.box-header -->
                <div class="card-body">
                    @foreach($categories as $category)
                        <div>{{ $category->id }} - {{ $category->category_name }}</div>

                        @if($category->products)
                            <ul>
                                @foreach($category->products as $product)
                                    <li>{{ $product->id }} - {{ $product->product_name }} - {{ $product->product_price }}</li>
                                    @if($product->ingredients)
                                        <ul>
                                            @foreach($product->ingredients as $ingredient)
                                                <li>{{ $ingredient->ingredient_name }}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                @endforeach
                            </ul>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop
