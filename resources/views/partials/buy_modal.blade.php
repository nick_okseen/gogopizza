<div class="modal-custom-header" data-current-product-id="{{ $current_product_id }}">
    <div class="left-side">
        <div class="title">{{ $category_name }}</div>
    </div>
    <div class="right-side">
        <div class="count">
            <button class="general_plus">+</button>
            <span class="product_count">
                <span class="count-inner">1</span> шт.
            </span>
            <button class="general_minus">-</button>
        </div>
        <div class="product-price">
            <span class="product-price-number">
                <span class="prize">{{ $modal_summa }}</span> грн.
            </span>
        </div>
    </div>
</div>
<div class="size_and_border">
    <div class="sizes">
        <div class="checkbox size">
            <input type="radio" class="pizza_name" name="size" value="{{ $first_product_id }}" {{ $check_first ? 'checked' : '' }}>
            <input type="hidden" class="pizza_count" value="1">
            <span class="single_price">
                <span class="single_price_price">{{ $first_product_price }}</span> грн.
            </span>
            <span class="pizza_size">{{ $first_product_name }}</span>
        </div>
        <div class="checkbox size">
            <input type="radio" class="pizza_name" name="size" value="{{ $second_product_id }}" {{ $check_second ? 'checked' : '' }}>
            <input type="hidden" class="pizza_count" value="1">
            <span class="single_price">
                <span class="single_price_price">{{ $second_product_price }}</span> грн.
            </span>
            <span class="pizza_size">{{ $second_product_name }}</span>
        </div>
    </div>
    <div class="pizza-border">
        <div class="checkbox borders">
            <input type="checkbox" name="borders" class="borders_input">

            <span class="single_price_price" data-mod-border="{{ $mod_with_border_id }}">
                <span class="prod_prise">{{ $mod_with_border_price }}</span> грн.
            </span>
            <span class="board_name">{{ $mod_name }} cм.</span>
        </div>
    </div>
</div>
<div class="additional">
    <ul style="columns: 2;">
        @foreach($additionals as $additional)
            <li>
                <input type="checkbox" name="dop[]" value="" class="additional_checkbox">
                <input type="hidden" value="1" class="add_quantity">

                <span>
                    <button class="additional_plus">+</button>
                    <span type="text" class="modif_count">
                            +<span>{{ $additional['price'] }}</span> грн.
                    </span>
                    <button class="additional_minus">-</button>
                </span>

                <span class="addit_count">1 шт. </span>
                <span class="dop_title">{{ $additional['name'] }}</span>
            </li>
        @endforeach
    </ul>
</div>

<div class="custom-footer">
    <button class="add-to-cart-modal" data-product-id="{{ $current_product_id }}">В корзину</button>
</div>
