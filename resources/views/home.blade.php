@extends('layouts.app')

@section('content')
<div class="container">
    @if($pizzaCategory->children)
        <div class="row">
            <div class="col-12">
                {{ $pizzaCategory->category_name }}
            </div>

            @foreach($pizzaCategory->children as $category)
                <div class="col-3 mb-4">
                    <div class="category_image">
                        <div style="width: 100%; height: 150px; background: grey;"></div>
                    </div>
                    <div>{{ $category->category_name }}</div>

                    @if($category->products)
                        @foreach($category->products as $product)
                            <div style="display: flex; align-items: center;justify-content: space-between;">
                                <div>{{ str_contains($product->product_name, '30') ? '30 см.' : '45 см.' }}</div>
                                <div>{{ $product->cost / 100 }} грн</div>
                                <div>
                                    <button class="add-to-cart-button" data-product-id="{{ $product->product_id }}">В корзину</button>
                                </div>
                            </div>
                            @if($loop->last)
                                @if($product->ingredients)
                                    @foreach($product->ingredients as $ingredient)
                                        @if($ingredient->is_in_products_list)
                                            {{ $ingredient->site_name }},
                                        @endif
                                    @endforeach
                                @endif
                            @endif
                        @endforeach
                    @endif
                </div>
            @endforeach
        </div>
    @endif

    @if($waterCategories)
        <div class="row">
            <div class="col-12">
                Напитки
            </div>
            @foreach($waterCategories as $category)
                    @if($category->children)
                        @foreach($category->children as $child)
                            @if($child->products)
                                @foreach($child->products as $product)
                                    <div class="col-3 mb-4">
                                        <div class="category_image">
                                            <div style="width: 100%; height: 150px; background: grey;"></div>
                                        </div>
                                        <div>{{ $product->product_name }}</div>
                                        <div>{{ $product->cost / 100 }} грн</div>
                                        <div>
                                            <button data-product-id="{{ $product->product_id }}">В корзину</button>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        @endforeach
                    @else
                        @if($category->products)
                            @foreach($child->products as $product)
                                <div class="col-3 mb-4">
                                    <div class="category_image">
                                        <div style="width: 100%; height: 150px; background: grey;"></div>
                                    </div>
                                    <div>{{ $product->product_name }}</div>
                                    <div>{{ $product->cost / 100 }} грн</div>
                                    <div>
                                        <button data-product-id="{{ $product->product_id }}">В корзину</button>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    @endif
                @endforeach
        </div>
    @endif

    <div class="modal fade show" id="additionalModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer-scripts')
    <script>
        document.addEventListener('DOMContentLoaded', function(){
            $(".add-to-cart-button").click(function (e) {
                e.preventDefault();
                let product_id = $(this).data('product-id');

                $.ajax({
                    url: '{{ route('cart.getModal') }}',
                    method: "get",
                    data: {
                        id: product_id
                    },
                    success: function (response) {
                        let data = response.data;
                        $('#additionalModal .modal-body').html(data);
                        $('#additionalModal').modal('show');
                    }
                });
            });

            $('body').on('change', 'input[name="size"]', function (){
                let size = $('input[name="size"]:checked').val();
                let quantity = $('.right-side .product_count .count-inner').text();

                $.ajax({
                    url: '{{ route('cart.getModal') }}',
                    method: "get",
                    data: {
                        id: size,
                        quantity: quantity
                    },
                    success: function (response) {
                        let data = response.data;
                        $('#additionalModal .modal-body').html(data);
                    }
                });
            });

            $('body').on('click', '.right-side .general_plus', function (){
                let quantity = $('.right-side .product_count .count-inner').text();
                quantity++;
                $('.right-side .product_count .count-inner').text(quantity);

                let prod_prise = $('.pizza_name:checked').closest('.size').find('.single_price_price').text();
                let sum = parseInt($('.product-price-number .prize').text());
                let borderCheckboxStatus = $('.borders_input').prop('checked');
                let border_sum = 0;
                let additional_sum = 0;

                if(borderCheckboxStatus){
                    border_sum += $('.single_price_price .prod_prise').text();
                }

                let addtionalChecks = $('.additional_checkbox:checked');
                addtionalChecks.each(function() {
                    let val = $(this).closest('li').find('.add_quantity').val();
                    let price = $(this).closest('li').find('.modif_count span').text();

                    additional_sum += (val * price);
                });

                sum = parseInt(prod_prise)*quantity + parseInt(border_sum)*quantity + parseInt(additional_sum)*quantity;

                $('.product-price-number .prize').text(sum);
            });

            $('body').on('click', '.right-side .general_minus', function (){
                let quantity = $('.right-side .product_count .count-inner').text();
                if(parseInt(quantity) > 1){
                    quantity--;
                }
                $('.right-side .product_count .count-inner').text(quantity);

                let prod_prise = $('.pizza_name:checked').closest('.size').find('.single_price_price').text();
                let sum = parseInt($('.product-price-number .prize').text());
                let borderCheckboxStatus = $('.borders_input').prop('checked');
                let border_sum = 0;
                let additional_sum = 0;

                if(borderCheckboxStatus){
                    border_sum += $('.single_price_price .prod_prise').text();
                }

                let addtionalChecks = $('.additional_checkbox:checked');
                addtionalChecks.each(function() {
                    let val = $(this).closest('li').find('.add_quantity').val();
                    let price = $(this).closest('li').find('.modif_count span').text();

                    additional_sum += (val * price);
                });

                sum = parseInt(prod_prise)*quantity + parseInt(border_sum)*quantity + parseInt(additional_sum)*quantity;

                $('.product-price-number .prize').text(sum);
            });

            $('body').on('change', '.borders_input', function (){
                let quantity = $('.right-side .product_count .count-inner').text();

                let prod_prise = $('.pizza_name:checked').closest('.size').find('.single_price_price').text();
                let sum = parseInt($('.product-price-number .prize').text());
                let borderCheckboxStatus = $('.borders_input').prop('checked');
                let border_sum = 0;
                let additional_sum = 0;

                if(borderCheckboxStatus){
                    border_sum += $('.single_price_price .prod_prise').text();
                }

                let addtionalChecks = $('.additional_checkbox:checked');
                addtionalChecks.each(function() {
                    let val = $(this).closest('li').find('.add_quantity').val();
                    let price = $(this).closest('li').find('.modif_count span').text();

                    additional_sum += (val * price);
                });

                sum = parseInt(prod_prise)*quantity + parseInt(border_sum)*quantity + parseInt(additional_sum);

                $('.product-price-number .prize').text(sum);
            });

            $('body').on('change', '.additional_checkbox', function (){
                let quantity = $('.right-side .product_count .count-inner').text();

                let prod_prise = $('.pizza_name:checked').closest('.size').find('.single_price_price').text();
                let sum = parseInt($('.product-price-number .prize').text());
                let borderCheckboxStatus = $('.borders_input').prop('checked');
                let border_sum = 0;
                let additional_sum = 0;

                if(borderCheckboxStatus){
                    border_sum += $('.single_price_price .prod_prise').text();
                }

                let addtionalChecks = $('.additional_checkbox:checked');
                addtionalChecks.each(function() {
                    let val = $(this).closest('li').find('.add_quantity').val();
                    let price = $(this).closest('li').find('.modif_count span').text();

                    additional_sum += (val * price);
                });

                sum = parseInt(prod_prise)*quantity + parseInt(border_sum)*quantity + parseInt(additional_sum);

                $('.product-price-number .prize').text(sum);
            });

            $('body').on('click', '.additional_plus', function (){
                if($(this).closest('li').find('.additional_checkbox').prop('checked')){
                    let quantity = $('.right-side .product_count .count-inner').text();

                    let prod_prise = $('.pizza_name:checked').closest('.size').find('.single_price_price').text();
                    let sum = parseInt($('.product-price-number .prize').text());
                    let borderCheckboxStatus = $('.borders_input').prop('checked');
                    let border_sum = 0;
                    let additional_sum = 0;

                    if(borderCheckboxStatus){
                        border_sum += $('.single_price_price .prod_prise').text();
                    }

                    let val = $(this).closest('li').find('.add_quantity').val();
                    val = parseInt(val) + 1;
                    $(this).closest('li').find('.add_quantity').val(val);
                    $(this).closest('li').find('.addit_count').text(val + ' шт.');

                    let addtionalChecks = $('.additional_checkbox:checked');
                    addtionalChecks.each(function() {
                        let val = $(this).closest('li').find('.add_quantity').val();
                        let price = $(this).closest('li').find('.modif_count span').text();

                        additional_sum += (val * price);
                    });

                    sum = parseInt(prod_prise)*quantity + parseInt(border_sum)*quantity + parseInt(additional_sum)*quantity;

                    $('.product-price-number .prize').text(sum);
                }
            });

            $('body').on('click', '.additional_minus', function (){
                if($(this).closest('li').find('.additional_checkbox').prop('checked')){
                    let quantity = $('.right-side .product_count .count-inner').text();

                    let prod_prise = $('.pizza_name:checked').closest('.size').find('.single_price_price').text();
                    let sum = parseInt($('.product-price-number .prize').text());
                    let borderCheckboxStatus = $('.borders_input').prop('checked');
                    let border_sum = 0;
                    let additional_sum = 0;

                    if(borderCheckboxStatus){
                        border_sum += $('.single_price_price .prod_prise').text();
                    }

                    let val = $(this).closest('li').find('.add_quantity').val();
                    if(parseInt(val) > 1){
                        val = parseInt(val) - 1;
                        $(this).closest('li').find('.add_quantity').val(val);
                        $(this).closest('li').find('.addit_count').text(val + ' шт.');
                    }

                    let addtionalChecks = $('.additional_checkbox:checked');
                    addtionalChecks.each(function() {
                        let val = $(this).closest('li').find('.add_quantity').val();
                        let price = $(this).closest('li').find('.modif_count span').text();

                        additional_sum += (val * price);
                    });

                    sum = parseInt(prod_prise)*quantity + parseInt(border_sum)*quantity + parseInt(additional_sum)*quantity;

                    $('.product-price-number .prize').text(sum);
                }
            });
        });
    </script>
@endsection
