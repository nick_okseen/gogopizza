<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="logo">
                            LOGO
                        </div>
                        <div class="menu">
                            <ul>
                                <li>Главная</li>
                                <li>О нас</li>
                                <li>Доставка</li>
                                <li>Контакты</li>
                            </ul>
                        </div>
                        <div class="auth">
                            <ul>
                                @guest
                                    <li>
                                        <a class="nav-link" href="{{ route('login') }}">Вход</a>
                                    </li>
                                @else
                                    <li>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Выход
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>
                                    </li>
                                @endif
                                <li>Корзина</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <main class="content">
            @yield('content')
        </main>

        <footer>
            <div>Footer</div>
        </footer>
    </div>
</body>

@yield('footer-scripts')

</html>
