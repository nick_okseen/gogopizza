<?php

namespace App\Views\Menu\Filters;

use Illuminate\Support\Facades\Auth;
use JeroenNoten\LaravelAdminLte\Menu\Filters\FilterInterface;

class RoleMenuFilter implements FilterInterface
{
    public function transform($item)
    {
        if (isset($item['role'])) {
        	if (is_array($item['role'])) {
        		if (!Auth::user()->hasOneRole($item['role'])) {
        			return false;
        		}
        	} else {
        		if (! Auth::user()->hasRole($item['role'])) {
	        		return false;
	        	}
        	}
        }

        if (isset($item['permission']) && !Auth::user()->hasPermission($item['permission'])) {
            return false;
        }

        return $item;
    }
}
