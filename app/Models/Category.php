<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id',
        'category_name',
        'parent_category',
        'category_color',
        'category_hidden',
        'category_photo',
        'tax_id',
    ];

    public function tax()
    {
        return $this->hasOne('App\Models\Tax', 'tax_id', 'tax_id');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product', 'menu_category_id', 'category_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Category', 'parent_category', 'category_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Category', 'parent_category', 'category_id');
    }
}
