<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'value'
    ];


    public static function getCategoryByKey($key)
    {
        $setting = self::where('key', $key)->first();
        if(isset($setting)){
            if($key === 'water_category'){
                return explode(',', $setting->value);
            }
            if($key === 'ingredients'){
                return explode(',', $setting->value);
            }
            if($key === 'modifications'){
                return explode(',', $setting->value);
            }

            return $setting->value;
        }

        return false;
    }
}
