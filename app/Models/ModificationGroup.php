<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModificationGroup extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'dish_modification_group_id',
        'name',
        'num_min',
        'num_max',
    ];

    public function modifications()
    {
        return $this->hasMany('App\Models\Modification', 'dish_modification_group_id');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product');
    }
}
