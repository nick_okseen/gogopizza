<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'structure_id',
        'ingredient_id',
        'pr_in_clear',
        'pr_in_cook',
        'pr_in_fry',
        'pr_in_stew',
        'pr_in_bake',
        'structure_unit',
        'structure_type',
        'structure_brutto',
        'structure_netto',
        'structure_lock',
        'structure_selfprice',
        'structure_selfprice_netto',
        'ingredient_name',
        'ingredient_unit',
        'ingredient_weight',
        'ingredients_losses_clear',
        'ingredients_losses_cook',
        'ingredients_losses_fry',
        'ingredients_losses_stew',
        'ingredients_losses_bake',
    ];

    public function products()
    {
        return $this->belongsToMany('App\Models\Product');
    }
}
