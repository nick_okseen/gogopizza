<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Modification extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'dish_modification_id',
        'dish_modification_group_id',
        'name',
        'ingredient_id',
        'type',
        'brutto',
        'price',
        'photo_orig',
        'photo_large',
        'photo_small',
    ];

    public function modificationGroup()
    {
        return $this->belongsTo('App\Models\ModificationGroup');
    }
}
