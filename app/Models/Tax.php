<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    const TAX_TYPES = [
        1 => "налог с продаж",
        2 => "налог с оборота",
        3 => "НДС",
        4 => "без налога",
    ];

    const TAX_FISCAL = [
        "0" => "Да",
        "1" => "Нет",
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tax_id',
        'tax_name',
        'tax_value',
        'tax_type',
        'tax_fiscal',
    ];
}
