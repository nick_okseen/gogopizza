<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'barcode',
        'category_name',
        'hidden',
        'unit',
        'cost',
        'cost_netto',
        'fiscal',
        'menu_category_id',
        'workshop',
        'nodiscount',
        'photo',
        'photo_origin',
        'product_code',
        'product_id',
        'product_name',
        'sort_order',
        'tax_id',
        'product_tax_id',
        'type',
        'weight_flag',
        'color',
        'product_price',
        'ingredient_id',
        'cooking_time',
        'product_production_description',
        'fiscal_code',
    ];

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'menu_category_id', 'category_id');
    }

    public function ingredients()
    {
        return $this->belongsToMany('App\Models\Ingredient');
    }

    public function modificationGroups()
    {
        return $this->belongsToMany('App\Models\ModificationGroup');
    }
}
