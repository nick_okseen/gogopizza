<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WebhookController extends Controller{

    public function webhook(Request $request){
        if($request->has('object') && $request->input('object') === 'category'){
            $this->syncCategory($request);
        }

        return response()->json([
            'status' => 'accept'
        ], 200);
    }

    public function syncCategory($request){

        if($request->input('action') === 'changed' || $request->input('action') === 'added'){
            $url = 'https://joinposter.com/api/menu.getCategory'
                . '?token=548376:283516477997401a458598385154802a'
                . '&category_id='. $request->input('object_id');

            $category = json_decode($this->sendRequest($url));

            if($category->response){
                $model = Category::updateOrCreate(
                    ['category_id' => $category->response->category_id],
                    [
                        'category_name' => $category->response->category_name,
                        'category_photo' => $category->response->category_photo,
                        'parent_category' => $category->response->parent_category,
                        'category_color' => $category->response->category_color,
                        'category_hidden' => $category->response->category_hidden,
                        'tax_id' => $category->response->tax_id
                    ]
                );
            }
        }

        if ($request->input('action') === 'removed') {
            $model = Category::where('category_id', $request->input('object_id'))->first();
            $model->delete();
        }
    }

    function sendRequest($url, $type = 'get', $params = [], $json = false)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        if ($type == 'post' || $type == 'put') {
            curl_setopt($ch, CURLOPT_POST, true);

            if ($json) {
                $params = json_encode($params);

                curl_setopt($ch, CURLOPT_HTTPHEADER, [
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($params)
                ]);

                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            } else {
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
            }
        }

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Poster (http://joinposter.com)');

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }
}
