<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Models\Category;
use App\Models\Ingredient;
use App\Models\Modification;
use App\Models\Product;
use App\Models\Settings;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SettingsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        $categories = Category::where('parent_category', 0)->get();
        $modifications = Modification::all();

        $pizza_category = Settings::getCategoryByKey('pizza_category');
        $water_category = Settings::getCategoryByKey('water_category');
        $selectedWithoutBorder= Settings::getCategoryByKey('without_border');
        $selectedBorder30= Settings::getCategoryByKey('border_30');
        $selectedBorder45= Settings::getCategoryByKey('border_45');
        $selectedAdditional= Settings::getCategoryByKey('modifications');

        return view('admin.settings.index', [
            'categories' => $categories,
            'modifications' => $modifications,
            'pizza_category' => $pizza_category,
            'water_category' => $water_category,
            'selectedWithoutBorder' => $selectedWithoutBorder,
            'selectedBorder30' => $selectedBorder30,
            'selectedBorder45' => $selectedBorder45,
            'selectedAdditional' => $selectedAdditional,
        ]);
    }

    public function store(Request $request){
        if($request->has('pizza_category')){
            $model = Settings::updateOrCreate(
                ['key' => 'pizza_category'],
                [
                    'value' => $request->input('pizza_category')
                ]
            );
        }

        if($request->has('water_category')){
            $model = Settings::updateOrCreate(
                ['key' => 'water_category'],
                [
                    'value' => implode(',', $request->input('water_category'))
                ]
            );
        }

        if($request->has('without_border')){
            $model = Settings::updateOrCreate(
                ['key' => 'without_border'],
                [
                    'value' => $request->input('without_border')
                ]
            );
        }

        if($request->has('border_30')){
            $model = Settings::updateOrCreate(
                ['key' => 'border_30'],
                [
                    'value' => $request->input('border_30')
                ]
            );
        }

        if($request->has('border_45')){
            $model = Settings::updateOrCreate(
                ['key' => 'border_45'],
                [
                    'value' => $request->input('border_45')
                ]
            );
        }

        if($request->has('modifications')){
            $model = Settings::updateOrCreate(
                ['key' => 'modifications'],
                [
                    'value' => implode(',', $request->input('modifications'))
                ]
            );
        }

        return redirect()->route('settings.index');
    }
}
