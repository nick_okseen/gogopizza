<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Illuminate\View\View;

class AdminsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function dashboard()
    {
        return view('admin.main');
    }
}
