<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;;

use App\Models\Category;
use App\Models\Ingredient;
use App\Models\Modification;
use App\Models\ModificationGroup;
use App\Models\Product;
use App\Models\Tax;
use Illuminate\View\View;

class SyncPosterController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        return view('admin.sync.index');
    }

    public function syncAll()
    {
        $this->getPosterTaxes();
        $this->getPosterCategories();
        $this->getPosterProducts();
    }

    public function getPosterCategories()
    {
        $url = 'https://joinposter.com/api/menu.getCategories'
            . '?token=' .$this->posterToken;

        $categories = json_decode($this->sendRequest($url));

        if ($categories->response){
            $newIds = [];

            foreach ($categories->response as $category) {
                $newIds[] =  $category->category_id;
            }

            foreach (Category::whereNotIn('category_id', $newIds)->get() as $category) {
                $category->delete();
            }

            foreach ($categories->response as $category) {
                $model = Category::updateOrCreate(
                    ['category_id' => $category->category_id],
                    [
                        'category_name' => $category->category_name,
                        'category_photo' => $category->category_photo,
                        'parent_category' => $category->parent_category,
                        'category_color' => $category->category_color,
                        'category_hidden' => $category->category_hidden,
                        'tax_id' => $category->tax_id != 0 ? $category->tax_id : null
                    ]
                );
            }
        }
        return response()->json(['success' => true], 200);
    }

    public function getPosterTaxes()
    {
        $url = 'https://joinposter.com/api/finance.getTaxes'
            . '?token=' .$this->posterToken;

        $taxes = json_decode($this->sendRequest($url));

        if ($taxes->response){
            $newIds = [];

            foreach ($taxes->response as $tax) {
                $newIds[] =  $tax->tax_id;
            }

            foreach (Tax::whereNotIn('tax_id', $newIds)->get() as $tax) {
                $tax->delete();
            }

            foreach ($taxes->response as $tax) {
                $model = Tax::updateOrCreate(
                    ['tax_id' => $tax->tax_id],
                    [
                        'tax_name' => $tax->tax_name,
                        'tax_value' => $tax->tax_value,
                        'tax_type' => $tax->type,
                        'tax_fiscal' => $tax->fiscal
                    ]
                );
            }
        }
        return response()->json(['success' => true], 200);
    }

    public function getPosterProducts()
    {
        $url = 'https://joinposter.com/api/menu.getProducts'
            . '?token=' . $this->posterToken;

        $products = json_decode($this->sendRequest($url));

        if ($products->response){

            $newIds = [];

            foreach ($products->response as $product) {
                $newIds[] =  $product->product_id;
            }

            foreach (Product::whereNotIn('product_id', $newIds)->get() as $product) {
                $product->delete();
            }

            foreach ($products->response as $product) {
                $productModel = Product::updateOrCreate(
                    ['product_id' => $product->product_id],
                    [
                        'barcode' => $product->barcode,
                        'category_name' => $product->category_name,
                        'hidden' => $product->hidden,
                        'unit' => $product->unit,
                        'cost' => isset($product->spots) ? $product->spots[0]->price : $product->cost,
                        'cost_netto' => $product->cost_netto,
                        'fiscal' => $product->fiscal,
                        'menu_category_id' => $product->menu_category_id != 0 ? $product->menu_category_id : null,
                        'workshop' => $product->workshop,
                        'nodiscount' => $product->nodiscount,
                        'photo' => $product->photo ? $product->photo : null,
                        'photo_origin' => $product->photo_origin ? $product->photo_origin : null,
                        'product_code' => $product->product_code,
                        'product_id' => $product->product_id,
                        'product_name' => $product->product_name,
                        'sort_order' => $product->sort_order,
                        'tax_id' => $product->tax_id,
                        'product_tax_id' => $product->product_tax_id,
                        'type' => $product->type,
                        'weight_flag' => $product->weight_flag,
                        'color' => $product->color,
                        'product_price' => $product->spots[0]->price,
                        'ingredient_id' => $product->ingredient_id,
                        'cooking_time' => isset($product->cooking_time) ? $product->cooking_time : 0,
                        'product_production_description' => isset($product->product_production_description) ? $product->product_production_description : '',
                        'fiscal_code' => isset($product->fiscal_code) ? $product->fiscal_code : '',
                    ]
                );

                if(isset($product->ingredients)) {
                    $ingredientsIds = [];

                    foreach ($product->ingredients as $ingredient) {
                        $ingredientModel = Ingredient::updateOrCreate(
                            ['ingredient_id' => $ingredient->ingredient_id],
                            [
                                'structure_id' => $ingredient->structure_id,
                                'pr_in_clear' => $ingredient->pr_in_clear,
                                'pr_in_cook' => $ingredient->pr_in_cook,
                                'pr_in_fry' => $ingredient->pr_in_fry,
                                'pr_in_stew' => $ingredient->pr_in_stew,
                                'pr_in_bake' => $ingredient->pr_in_bake,
                                'structure_unit' => $ingredient->structure_unit,
                                'structure_type' => $ingredient->structure_type,
                                'structure_brutto' => $ingredient->structure_brutto,
                                'structure_netto' => $ingredient->structure_netto,
                                'structure_lock' => $ingredient->structure_lock,
                                'structure_selfprice' => $ingredient->structure_selfprice,
                                'structure_selfprice_netto' => $ingredient->structure_selfprice_netto,
                                'ingredient_name' => $ingredient->ingredient_name,
                                'ingredient_unit' => $ingredient->ingredient_unit,
                                'ingredient_weight' => $ingredient->ingredient_weight,
                                'ingredients_losses_clear' => $ingredient->ingredients_losses_clear,
                                'ingredients_losses_cook' => $ingredient->ingredients_losses_cook,
                                'ingredients_losses_fry' => $ingredient->ingredients_losses_fry,
                                'ingredients_losses_stew' => $ingredient->ingredients_losses_stew,
                                'ingredients_losses_bake' => $ingredient->ingredients_losses_bake,
                            ]
                        );

                        $ingredientsIds[] = $ingredientModel->id;
                    }

                    $productModel->ingredients()->sync($ingredientsIds);
                }

                if(isset($product->group_modifications)) {
                    $groupModificationsIds = [];

                    foreach ($product->group_modifications as $groupModification) {
                        $groupModificationModel = ModificationGroup::updateOrCreate(
                            ['dish_modification_group_id' => $groupModification->dish_modification_group_id],
                            [
                                'name' => $groupModification->name,
                                'num_min' => $groupModification->num_min,
                                'num_max' => $groupModification->num_max,
                            ]
                        );

                        if(isset($groupModification->modifications)) {
                            foreach ($groupModification->modifications as $modification) {
                                $modificationModel = Modification::updateOrCreate(
                                    ['dish_modification_id' => $modification->dish_modification_id],
                                    [
                                        'dish_modification_group_id' => $groupModificationModel->id,
                                        'name' => $modification->name,
                                        'ingredient_id' => $modification->ingredient_id,
                                        'type' => $modification->type,
                                        'brutto' => $modification->brutto,
                                        'price' => $modification->price,
                                        'photo_orig' => $modification->photo_orig,
                                        'photo_large' => $modification->photo_large,
                                        'photo_small' => $modification->photo_small,
                                    ]
                                );
                            }
                        }

                        $groupModificationsIds[] = $groupModificationModel->id;
                    }

                    $productModel->modificationGroups()->sync($groupModificationsIds);
                }
            }
        }
        return response()->json(['success' => true], 200);
    }
}
