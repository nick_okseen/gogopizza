<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Models\Category;
use App\Models\Ingredient;
use App\Models\Product;
use App\Models\Settings;
use Illuminate\Http\Request;
use Illuminate\View\View;

class IngredientsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        $ingredients = Ingredient::all();

        return view('admin.ingredients.index', [
            'ingredients' => $ingredients
        ]);
    }

    public function updateIngredient(Request $request){
        $ingredient = Ingredient::where('id', $request->input('id'))->first();

        if($request->has('site_name')){
            $ingredient->site_name = $request->input('site_name');
        }

        if($request->has('is_in_products_list')){
            $ingredient->is_in_products_list = $request->input('is_in_products_list') === 'true' ? 1 : 0;
        }

        $ingredient->save();
    }
}
