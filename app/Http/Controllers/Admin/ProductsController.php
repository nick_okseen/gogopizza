<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Models\Category;
use App\Models\Product;
use App\Models\Settings;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ProductsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */

    public function index()
    {
        $pizzaCategoryId = Settings::getCategoryByKey('pizza_category');

        if($pizzaCategoryId){
            $parentCategory = Category::where('category_id', $pizzaCategoryId)->first();

            $products = collect();
            foreach($parentCategory->children as $category){
                $products = $products->merge($category->products);
            }
        }

        return view('admin.products.index', [
            'products' => $products ?? []
        ]);
    }

    public function updateProduct(Request $request){
        $product = Product::where('id', $request->input('id'))->first();

        if($request->has('is_30')){
            $product->is_30 = $request->input('is_30') === 'true' ? 1 : 0;
        }
        if($request->has('is_45')){
            $product->is_45 = $request->input('is_45') === 'true' ? 1 : 0;
        }

        $product->save();
    }
}
