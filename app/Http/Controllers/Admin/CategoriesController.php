<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Models\Category;
use App\Models\Product;
use App\Models\Settings;
use Illuminate\View\View;

class CategoriesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        $categoryKey = Settings::getCategoryByKey('pizza_category');

        if($categoryKey){
            $parentCategory = Category::where('category_id', $categoryKey)->first();
            $categories = Category::where('parent_category', $parentCategory->category_id)->get();
        }

        return view('admin.categories.index', [
            'categories' => $categories ?? []
        ]);
    }
}
