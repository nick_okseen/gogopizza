<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Models\Category;
use App\Models\Product;
use App\Models\Settings;
use Illuminate\View\View;

class SalesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        $url = 'https://joinposter.com/api/incomingOrders.createIncomingOrder'
            . '?token=' . $this->posterToken;

        $incoming_order = [
            "spot_id"   => 1,
            "phone"   => '0533466554',
            "products"  => [
                [
                    "product_id"    => 7,
                    "count"         => 1
                ],
            ],
            "promotion" => [
                [
                    "id" => 3,
                    "involved_products" => [
                        [
                            "id" => 7,
                            "count"  => 1
                        ]
                    ]
                ]
            ]
        ];

        $data = json_decode($this->sendRequest($url, 'post', $incoming_order));
        dd($data);

//        $url = 'https://joinposter.com/api/clients.getPromotions'
//            . '?token=' . $this->posterToken;
//
//        $sales = json_decode($this->sendRequest($url));

        return view('admin.sales.index', [
            'sales' => $sales ?? []
        ]);
    }
}
