<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\View\View;

class OrdersController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        return view('admin.orders.index');
    }

    public function makeTestOrder(Request $request)
    {
        $url = 'https://joinposter.com/api/incomingOrders.createIncomingOrder'
            . '?token=' . $this->posterToken;

        $incoming_order = [
            'spot_id'   => 1,
            'phone'     => '+380680000066',
            'first_name'     => 'first name 6',
            'last_name'     => 'last name 6',
            'email'     => 'test6@test.com',
            'sex'     => '1',
            'products'  => [
                [
                    'product_id' => 46,
                    'count'      => 2,
                    'modification' => [
                        [
                            'm' => 8,
                            'a' => 1
                        ],
                        [
                            'm' => 10,
                            'a' => 2
                        ],
                        [
                            'm' => 11,
                            'a' => 3
                        ],
                    ]
                ],
                [
                    'product_id' => 47,
                    'count'      => 1,
                ],
            ],
        ];

        $request->session()->getId();

        $data = json_decode($this->sendRequest($url, 'post', $incoming_order));
        dd($data);
    }

    public function getPosterOrder(){
        $url = 'https://joinposter.com/api/incomingOrders.getIncomingOrder'
            . '?token=' . $this->posterToken
            . '&incoming_order_id=11';

        $data = json_decode($this->sendRequest($url));
        dd($data);
    }
}
