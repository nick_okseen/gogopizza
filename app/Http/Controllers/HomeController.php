<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class HomeController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $pizzaCategoryKey = Settings::getCategoryByKey('pizza_category');
        $pizzaCategory = Category::where('category_id', $pizzaCategoryKey)->first();

        $waterCategoryKey = Settings::getCategoryByKey('water_category');
        $waterCategories = Category::whereIn('category_id', $waterCategoryKey)->get();

        return view('home', [
            'pizzaCategory' => $pizzaCategory,
            'waterCategories' => $waterCategories
        ]);
    }
}
