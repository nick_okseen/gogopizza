<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Modification;
use App\Models\Product;
use App\Models\Settings;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CartController extends BaseController
{
    public function cartModal(Request $request){
        $dataForRender = [];

        $currentProduct = Product::where('product_id', $request->input('id'))->first();
        $currentProductCategory = $currentProduct->category;
        $subProduct = Product::where('menu_category_id', $currentProductCategory->category_id)
            ->where('product_id', '<>', $currentProduct->product_id)->first();

        $modWithoutBorder = Settings::getCategoryByKey('without_border');
        $mod30 = Settings::getCategoryByKey('border_30');
        $mod45 = Settings::getCategoryByKey('border_45');
        $modAdditionals = Settings::getCategoryByKey('modifications');

        $additionals = Modification::whereIn('dish_modification_id', $modAdditionals)->get();
        $modWithout = Modification::where('dish_modification_id', $modWithoutBorder)->first();
        $mod30Model = Modification::where('dish_modification_id', $mod30)->first();
        $mod45Model = Modification::where('dish_modification_id', $mod45)->first();

        $dataForRender['without_border_id'] = $modWithout->dish_modification_id;
        $dataForRender['without_border_name'] = $modWithout->name;
        $dataForRender['without_border_price'] = $modWithout->price;
        $dataForRender['category_name'] = $currentProduct->category_name;

        $dataForRender['modal_summa'] = $currentProduct->cost / 100;

        if($currentProduct->is_30){
            $dataForRender['current_product_id'] = $currentProduct->product_id;

            $dataForRender['first_product_id'] = $currentProduct->product_id;
            $dataForRender['first_product_name'] = $currentProduct->product_name;
            $dataForRender['first_product_price'] = $currentProduct->cost / 100;
            $dataForRender['second_product_id'] = $subProduct->product_id;
            $dataForRender['second_product_name'] = $subProduct->product_name;
            $dataForRender['second_product_price'] = $subProduct->cost / 100;
            $dataForRender['check_first'] = true;
            $dataForRender['check_second'] = false;
            $dataForRender['mod_name'] = '30';

            $dataForRender['mod_with_border_id'] = $mod30Model->id;
            $dataForRender['mod_with_border_name'] = $mod30Model->name;
            $dataForRender['mod_with_border_price'] = $mod30Model->price;
        } else {
            $dataForRender['current_product_id'] = $currentProduct->product_id;

            $dataForRender['first_product_id'] = $subProduct->product_id;
            $dataForRender['first_product_name'] = $subProduct->product_name;
            $dataForRender['first_product_price'] = $subProduct->cost / 100;
            $dataForRender['second_product_id'] = $currentProduct->product_id;
            $dataForRender['second_product_name'] = $currentProduct->product_name;
            $dataForRender['second_product_price'] = $currentProduct->cost / 100;
            $dataForRender['check_first'] = false;
            $dataForRender['check_second'] = true;
            $dataForRender['mod_name'] = '45';

            $dataForRender['mod_with_border_id'] = $mod45Model->id;
            $dataForRender['mod_with_border_name'] = $mod45Model->name;
            $dataForRender['mod_with_border_price'] = $mod45Model->price;
        }

        foreach ($additionals as $additional) {
            $dataForRender['additionals'][] = [
                'id' => $additional->dish_modification_id,
                'price' => $additional->price,
                'name' => $additional->name,
            ];
        }

        $view = view('partials.buy_modal', $dataForRender, ['version' => '1.0.0'])->render();
        return response()->json([
            'data' => $view
        ], 200);
    }

    public function addToCart($id)
    {
        $product = Product::where('product_id', $id);
        if(!$product) {
            abort(404);
        }
        $cart = session()->get('cart');
        // if cart is empty then this the first product
        if(!$cart) {
            $cart = [
                $id => [
                    "name" => $product->product_name,
                    "quantity" => 1,
                    "price" => $product->cost / 100,
                    "photo" => $product->photo
                ]
            ];
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }
        // if cart not empty then check if this product exist then increment quantity
        if(isset($cart[$id])) {
            $cart[$id]['quantity']++;
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }
        // if item not exist in cart then add to cart with quantity = 1
        $cart[$id] = [
            "name" => $product->product_name,
            "quantity" => 1,
            "price" => $product->cost / 100,
            "photo" => $product->photo
        ];
        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }
}
