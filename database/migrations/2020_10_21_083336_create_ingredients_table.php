<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredients', function (Blueprint $table) {
            $table->id();
            $table->integer('structure_id');
            $table->unsignedBigInteger('ingredient_id')->unique();
            $table->integer('pr_in_clear');
            $table->integer('pr_in_cook');
            $table->integer('pr_in_fry');
            $table->integer('pr_in_stew');
            $table->integer('pr_in_bake');
            $table->string('structure_unit');
            $table->integer('structure_type');
            $table->string('structure_brutto');
            $table->string('structure_netto');
            $table->integer('structure_lock');
            $table->integer('structure_selfprice');
            $table->integer('structure_selfprice_netto');
            $table->string('ingredient_name');
            $table->string('ingredient_unit');
            $table->string('ingredient_weight');
            $table->string('ingredients_losses_clear')->nullable();
            $table->string('ingredients_losses_cook')->nullable();
            $table->string('ingredients_losses_fry')->nullable();
            $table->string('ingredients_losses_stew')->nullable();
            $table->string('ingredients_losses_bake')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredients');
    }
}
