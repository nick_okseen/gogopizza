<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('barcode');
            $table->string('category_name');
            $table->integer('hidden');
            $table->string('unit');
            $table->integer('cost');
            $table->integer('cost_netto');
            $table->integer('fiscal');
            $table->unsignedBigInteger('menu_category_id')->nullable();
            $table->integer('workshop');
            $table->integer('nodiscount');
            $table->text('photo')->nullable();
            $table->text('photo_origin')->nullable();
            $table->text('product_code');
            $table->unsignedBigInteger('product_id')->unique();
            $table->string('product_name');
            $table->integer('sort_order');
            $table->integer('tax_id');
            $table->integer('product_tax_id');
            $table->integer('type');
            $table->integer('weight_flag');
            $table->string('color');
            $table->integer('product_price');
            $table->integer('ingredient_id')->nullable();
            $table->integer('cooking_time');
            $table->text('product_production_description');
            $table->text('fiscal_code');

            $table->foreign('menu_category_id')
                ->references('category_id')
                ->on('categories')
                ->onDelete('SET NULL');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'products', function ( Blueprint $table ) {
            $table->dropForeign(['menu_category_id']);
        });

        Schema::dropIfExists('products');
    }
}
