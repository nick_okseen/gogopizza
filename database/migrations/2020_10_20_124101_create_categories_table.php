<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id')->unique();
            $table->text('category_name');
            $table->text('category_photo')->nullable();
            $table->integer('parent_category')->default(0);
            $table->string('category_color')->nullable();
            $table->tinyInteger('category_hidden')->default(0);
            $table->unsignedBigInteger('tax_id')->nullable();
            $table->foreign('tax_id')
                ->references('tax_id')
                ->on('taxes')
                ->onDelete('SET NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'categories', function ( Blueprint $table ) {
            $table->dropForeign(['tax_id']);
        });

        Schema::dropIfExists('categories');
    }
}
