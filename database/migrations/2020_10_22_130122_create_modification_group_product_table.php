<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModificationGroupProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modification_group_product', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('modification_group_id');
            $table->unsignedBigInteger('product_id');

            $table->foreign('modification_group_id')
                ->references('id')
                ->on('modification_groups')
                ->onDelete('CASCADE');

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('CASCADE');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'modification_group_product', function ( Blueprint $table ) {
            $table->dropForeign(['modification_group_id', 'product_id']);
        });

        Schema::dropIfExists('modification_group_product');
    }
}
