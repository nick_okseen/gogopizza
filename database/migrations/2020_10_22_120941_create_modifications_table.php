<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modifications', function (Blueprint $table) {
            $table->id();
            $table->integer('dish_modification_id');
            $table->unsignedBigInteger('dish_modification_group_id')->nullable();
            $table->string('name');
            $table->integer('ingredient_id');
            $table->integer('type');
            $table->integer('brutto');
            $table->integer('price')->nullable();
            $table->text('photo_orig')->nullable();
            $table->text('photo_large')->nullable();
            $table->text('photo_small')->nullable();

            $table->foreign('dish_modification_group_id')
                ->references('id')
                ->on('modification_groups')
                ->onDelete('SET NULL');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'modifications', function ( Blueprint $table ) {
            $table->dropForeign(['dish_modification_group_id']);
        });

        Schema::dropIfExists('modifications');
    }
}
