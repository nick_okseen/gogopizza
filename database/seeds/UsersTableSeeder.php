<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\User::all();
        foreach ($users as $user) {
            $user->delete();
        }

        $roleAdmin = config('roles.models.role')::where('slug', '=', 'admin')->first();
        $roleOperator = config('roles.models.role')::where('slug', '=', 'operator')->first();


        $users = [
            [
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'password' => '11111111',
            ],
            [
                'name' => 'Moderator',
                'email' => 'moderator@moderator.com',
                'password' => '11111111',
            ],
            [
                'name' => 'User',
                'email' => 'user@user.com',
                'password' => '11111111',
            ],
        ];

        foreach ($users as $data) {
            $user = \App\User::create(
                [
                    'name'      => $data['name'],
                    'email'      => $data['email'],
                    'password'   => Hash::make($data['password']),
                ]
            );

            $role = config('roles.models.role')::where('name', '=', $data['name'])->first();
            $user->attachRole($role);
        }
    }
}
