<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('cart-modal', 'CartController@cartModal')->name('cart.getModal');
Route::get('add-to-cart/{id}', 'CartController@addToCart');

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' => [
        'auth',
        'level:3',
    ]
], function () {
    Route::get('dashboard', 'AdminsController@dashboard')->name('dashboard');

    Route::get('sync', 'SyncPosterController@index')->name('poster.index');
    Route::get('sync-all', 'SyncPosterController@syncAll')->name('poster.syncAll');

    Route::get('products', 'ProductsController@index')->name('products.index');
    Route::post('products/update-product', 'ProductsController@updateProduct')->name('products.update');

    Route::get('categories', 'CategoriesController@index')->name('categories.index');

    Route::get('sales', 'SalesController@index')->name('sales.index');

    Route::get('orders', 'OrdersController@index')->name('orders.index');
    Route::get('orders/make-test-order', 'OrdersController@makeTestOrder')->name('orders.makeTestOrder');
    Route::get('orders/get-poster-order', 'OrdersController@getPosterOrder')->name('orders.getPosterOrder');

    Route::get('settings', 'SettingsController@index')->name('settings.index');
    Route::post('settings/store', 'SettingsController@store')->name('settings.store');

    Route::get('ingredients', 'IngredientsController@index')->name('ingredients.index');
    Route::post('ingredients/update-ingredient', 'IngredientsController@updateIngredient')->name('ingredients.update');
});
